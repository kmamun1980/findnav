"""
WSGI config for findnav project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys
sys.path.append('/home/mamun/workspace/findnav/')
print sys.path
print ("-------")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "findnav.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
